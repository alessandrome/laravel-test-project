
## Laravel Test
  
Laravel simple install & configuration for testing it.
Laravel project configured to use **Ngnix** server and **MySQL** DB.
Has been created a User Table and simple login test with the help of **"artisan"** command.

---

## Dependencies

Laravel need some dependencies to work wich are downloadable with **_composer_** with the following command:  
```
 composer install
```

To restore all needed database tables use the command:  
```
 php artisan migrate
```

These commands need to be launched from the laravel project root directory containing the _composer.json_ and _composer.lock_ files.  
The shell script 'install\_dependencie.sh' contains the previous commands.

---

## Test Box

This Laravel project has been tested on the virtual box [Homestead](https://laravel.com/docs/5.5/homestead) using [Vagrant](www.vagrantup.com) with a [VirtualBox environment](https://www.virtualbox.org/).
The virtual machine has been used both with Vagrant SSH access and with VirtualBox GUI.
